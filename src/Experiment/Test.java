package Experiment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) {
//        RandomClass random = null;
//        try {
//            random = new RandomClass(-1);
//            System.out.println(random);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        } finally {
//            System.out.println(random);
//        }

//        double d = 1.5;
//        System.out.println(d - (long) d);

        List<Integer> rndInts = new ArrayList<>();
        rndInts.add(1);
        rndInts.add(2);
        rndInts.add(3);
        rndInts.add(4);
        rndInts.add(5);
        int tmp = rndInts.get(1);
        rndInts.set(1,rndInts.get(3));
        rndInts.set(3, tmp);
        System.out.println(rndInts);
        rndInts.add(1, 0);
        System.out.println(rndInts);
    }
}
