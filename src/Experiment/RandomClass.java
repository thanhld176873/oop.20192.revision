package Experiment;

public class RandomClass {
    private int attr;

    public RandomClass(int attr) {
        if (attr < 0) throw new IllegalArgumentException();
        this.attr = attr;
    }
}
