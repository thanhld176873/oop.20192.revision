package Final_20182.document_components;

import Final_20182.decoration.Frame;

public class MediaBaseComponent extends DocumentComponent {
    protected String url;
    protected double absoluteSize;
    protected double relativeSize;
    protected Text caption;
    protected Frame frame;

    protected MediaBaseComponent(String url, double absoluteSize, double relativeSize, Text caption, Frame frame) {
        super();
        this.url = url;
        this.absoluteSize = absoluteSize;
        this.relativeSize = relativeSize;
        this.caption = caption;
        this.frame = frame;
    }

    // getters and setters

    @Override
    public String toString() {
        return "MediaBaseComponent{" +
                "url='" + url + '\'' +
                ", absoluteSize=" + absoluteSize +
                ", relativeSize=" + relativeSize +
                ", caption=" + caption.toString() +
                ", frame=" + frame.toString() +
                '}';
    }
}
