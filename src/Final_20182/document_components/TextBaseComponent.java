package Final_20182.document_components;

import Final_20182.decoration.Color;
import Final_20182.decoration.Font;

public class TextBaseComponent extends DocumentComponent {
    protected String content;
    protected Font font;
    protected Color fontColor;
    protected Color backgroundColor;

    public TextBaseComponent(String content, Font font, Color fontColor, Color backgroundColor) {
        super();
        this.content = content;
        this.font = font;
        this.fontColor = fontColor;
        this.backgroundColor = backgroundColor;
    }

    // getters and setters

    @Override
    public String toString() {
        return "TextBaseComponent{" +
                "content='" + content + '\'' +
                ", font=" + font +
                ", fontColor=" + fontColor +
                ", backgroundColor=" + backgroundColor +
                '}';
    }
}
