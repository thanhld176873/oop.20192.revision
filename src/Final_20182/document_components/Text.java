package Final_20182.document_components;

import Final_20182.decoration.Color;
import Final_20182.decoration.Font;

public class Text extends TextBaseComponent {
    private double fontSize;

    public Text(String content, Font font, Color fontColor, Color backgroundColor, double fontSize) {
        super(content, font, fontColor, backgroundColor);
        double fraction = fontSize - (long) fontSize;
        if (fontSize < 8 || fontSize > 20 || (fraction != 0.0 && fraction != 0.5)) throw new IllegalArgumentException();
        this.fontSize = fontSize;
    }

    @Override
    public String toString() {
        return "Text{" +
                super.toString() +
                "fontSize=" + fontSize +
                '}';
    }
}
