package Final_20182.document_components;

import Final_20182.decoration.Color;
import Final_20182.decoration.Font;

public class ArtText extends TextBaseComponent {
    private int fontSize;

    public ArtText(String content, Font font, Color fontColor, Color backgroundColor, int fontSize) {
        super(content, font, fontColor, backgroundColor);
        if (fontSize < 8 || fontSize > 20) throw new IllegalArgumentException();
        this.fontSize = fontSize;
    }

    @Override
    public String toString() {
        return "ArtText{" +
                super.toString() +
                "fontSize=" + fontSize +
                '}';
    }
}
