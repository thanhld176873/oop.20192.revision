package Final_20182.document_components;

import Final_20182.decoration.Frame;

public class Image extends MediaBaseComponent {
    private double transparent;
    private int brightness;

    public Image(String url, double absoluteSize, double relativeSize, Text caption, Frame frame, double transparent, int brightness) {
        super(url, absoluteSize, relativeSize, caption, frame);
        this.transparent = transparent;
        if (brightness < 0 || brightness > 100) throw new IllegalArgumentException();
        this.brightness = brightness;
    }

    @Override
    public String toString() {
        return "Image{" +
                super.toString() +
                "transparent=" + transparent +
                ", brightness=" + brightness +
                '}';
    }
}
