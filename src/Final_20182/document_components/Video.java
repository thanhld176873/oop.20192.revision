package Final_20182.document_components;

import Final_20182.decoration.Frame;

public class Video extends MediaBaseComponent {
    private boolean played;
    private int volume;

    public Video(String url, double absoluteSize, double relativeSize, Text caption, Frame frame, boolean played, int volume) {
        super(url, absoluteSize, relativeSize, caption, frame);
        this.played = played;
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Video{" +
                super.toString() +
                "played=" + played +
                ", volume=" + volume +
                '}';
    }
}
