package Final_20182;

import Final_20182.decoration.Color;
import Final_20182.decoration.Font;
import Final_20182.decoration.Frame;
import Final_20182.document_components.DocumentComponent;
import Final_20182.document_components.Image;
import Final_20182.document_components.Text;
import Final_20182.document_components.Video;

public class Main {

    public static void main(String[] args) {

        DocumentComponent dc1 = new Text("This is a text", Font.Arial, Color.GREEN, Color.BLUE, 12);
        DocumentComponent dc2 = new Text("This is an art text", Font.Handy, Color.BLUE, Color.GREEN, 14);
        DocumentComponent dc3 = new Image("C:\\img\\sample.png", 1080.0, 80.0, (Text) dc1, new Frame(Color.RED, 1.0, Frame.Style.Dashed), 80, 100);
        DocumentComponent dc4 = new Video("C:\\img\\sample.png", 1080.0, 80.0, (Text) dc1, new Frame(Color.RED, 1.0, Frame.Style.Dashed), false, 36);
        Document document = new Document();
        document.addComponent(dc1, dc2, dc3, dc4);
        System.out.println(document.toString());
        document.swapComponents(0,2);
        System.out.println(document.toString());

    }
}
