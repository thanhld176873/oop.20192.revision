package Final_20182.decoration;

public enum Font {
     TimesNewRoman,
     Arial,
     Fancy,
     Handy
}
