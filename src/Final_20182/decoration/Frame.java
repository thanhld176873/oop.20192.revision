package Final_20182.decoration;

public class Frame {

    public enum Style {Solid, Dashed, Dotted}
    private Color color;
    private double weight;
    private Style style;

    public Frame(Color color, double weight, Style style) {
        this.color = color;
        this.weight = weight;
        this.style = style;
    }

    @Override
    public String toString() {
        return "Frame{" +
                "color=" + color +
                ", weight=" + weight +
                ", style=" + style +
                '}';
    }
}
