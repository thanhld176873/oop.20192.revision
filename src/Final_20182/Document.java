package Final_20182;

import Final_20182.document_components.DocumentComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Document {
    private List<DocumentComponent> components = new ArrayList<>();

    Document() {
    }

    Document(List<DocumentComponent> components) {
        this.components = components;
    }

    boolean swapComponents(int idx1, int idx2) {
        try {
            DocumentComponent tmp = components.get(idx1);
            components.set(idx1, components.get(idx2));
            components.set(idx2, tmp);
            return true;
        } catch(IndexOutOfBoundsException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    boolean addComponent(DocumentComponent... dc) {
        return this.components.addAll(Arrays.asList(dc));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Document\n");
        components.forEach(c -> {
            sb.append(c.toString());
            sb.append("\n");
        });
        return sb.toString();

    }
}
