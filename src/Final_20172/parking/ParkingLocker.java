package Final_20172.parking;

import Final_20172.bike.Bike;

public class ParkingLocker {
    ParkingLot parkingLot;
    Bike lockedBike;

    public ParkingLocker(ParkingLot parkingLot, Bike lockedBike) {
        this.parkingLot = parkingLot;
        this.lockedBike = lockedBike;
    }

    public Bike getLockedBike() {
        return this.lockedBike;
    }

    public boolean lockBike(Bike bike) {
        if (this.lockedBike != null) return false;
        this.lockedBike = bike;
        return true;
    }

    public Bike unlockBike() {
        Bike bike = this.lockedBike;
        this.lockedBike = null;
        return bike;
    }
}
