package Final_20172.parking;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingLot {
    private String location;
    List<ParkingLocker> parkingLockers = new ArrayList<>();

    public List<ParkingLocker> getAvailableBikes() {
        return this.parkingLockers.stream()
                .filter(pl -> pl.getLockedBike() != null)
                .collect(Collectors.toList());
    }

}
