package Final_20172.user;

public class AppUser {
    private String username;
    private String password;
    private PaymentAccount account;

    public AppUser(String username, String password, PaymentAccount account) {
        this.username = username;
        this.password = password;
        this.account = account;
    }

    public String payRent(double rentCost) {
        return this.account.payRent(rentCost);
    }
}
