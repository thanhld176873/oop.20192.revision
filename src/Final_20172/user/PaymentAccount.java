package Final_20172.user;

public class PaymentAccount {

    private String creditNumber;
    private double balance;

    public PaymentAccount(String creditNumber, double balance) {
        this.creditNumber = creditNumber;
         this.balance = balance;
    }

    protected String payRent(double cost) {
        this.balance = this.balance - cost;
        if (this.balance > 0) return "Paid Successfully";
        else return "Your account is in debt";
    }
}
