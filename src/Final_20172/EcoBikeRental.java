package Final_20172;

import Final_20172.bike.Bike;
import Final_20172.parking.ParkingLocker;
import Final_20172.parking.RentingManager;
import Final_20172.user.AppUser;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class EcoBikeRental {
    public static AppUser currentUser;

    public static void login() {

    }

    public static void returnBike(Bike bike) {
        String msg = currentUser.payRent(bike.calculateRentalCost());
        System.out.println(msg);
        review();
        bike.setStartTime(null);
    }

    public static void rentBike(String stripedCode) {
        ParkingLocker locker = RentingManager.PARKING_LOTS
                .stream()
                .flatMap(parkingLot -> parkingLot.getAvailableBikes().stream())
                .filter(parkingLocker -> parkingLocker.getLockedBike().getStripedCode().equals(stripedCode))
                .findFirst()
                .orElse(null);
        if (locker == null) System.out.println("Internal System Error");
        else {
            Bike bike = locker.unlockBike();
            bike.setStartTime(LocalDateTime.now());
        }
    }

    public static void review() {

    }

    public static void main(String[] args) {

    }

}
