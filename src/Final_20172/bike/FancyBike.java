package Final_20172.bike;

import java.time.LocalDateTime;

public abstract class FancyBike extends Bike {

    protected FancyBike(int saddles, int backSeat, int pedals, String stripedCode) {
        super(saddles, backSeat, pedals, stripedCode);
    }

    @Override
    public double calculateRentalCost() {
        return super.calculateRentalCost() * 1.5;
    }
}
