package Final_20172.bike;

public class ElectricBike extends FancyBike {

    private double battery;
    private double power;
    private double longevity;

    public ElectricBike(double battery, double power, String stripedCode) {
        super(1,1,1, stripedCode);
        this.battery = battery;
        this.power = power;
        this.longevity = this.battery / this.power;
    }
}
