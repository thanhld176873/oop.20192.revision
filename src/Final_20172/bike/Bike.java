package Final_20172.bike;

import Final_20172.parking.ParkingLocker;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public abstract class Bike {

    protected int saddles;
    protected int backSeat;
    protected int pedals;
    protected String stripedCode;
    protected LocalDateTime startTime;

    protected Bike(int saddles, int backSeat, int pedals, String stripedCode) {
        this.saddles = saddles;
        this.backSeat = backSeat;
        this.pedals = pedals;
        this.stripedCode = stripedCode;
    }

    public String getStripedCode() {
        return this.stripedCode;
    }

    public LocalDateTime getStartTime() {
        return this.startTime;
    }

    public void setStartTime(LocalDateTime dateTime) {
        this.startTime = dateTime;
    }

    public double calculateRentalCost() {
        long time = ChronoUnit.MINUTES.between(this.startTime, LocalDateTime.now());
        if (time < 5) return 0;
        if (time < 30) return 15000;
        return 15000 + Math.ceil((time-30)/15.0) * 5000;
    }
}
