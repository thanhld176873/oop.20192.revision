package Final_20172.bike;

public class DoubleBike extends FancyBike {

    public DoubleBike(String stripedCode) {
        super(2,1,2, stripedCode);
    }
}
