package Final_20172;

import Final_20172.parking.ParkingLocker;

public class EcoBikerLockerApp {
    private static ParkingLocker locker;

    private static boolean checkLocker() {
        while (locker.getLockedBike() == null);
        return true;
    }

    public static void main(String[] args) {
        while (true) {
            if (checkLocker() && locker.getLockedBike().getStartTime() != null) {
                EcoBikeRental.returnBike(locker.getLockedBike());
            }
        }
    }


}
